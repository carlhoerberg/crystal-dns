require "spec"
require "../src/dns"
include DNS

describe Server do
  it "responds" do
    server = Server.new Socket::IPAddress.new("127.0.0.1", 4562)
    server.start
    config = Resolver::Config.new [Resolver::NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("127.0.0.1", 4562))]
    resolver = Resolver.new config
    response = resolver.query("www.example.com", RecordType::A)
    response.answers[0].name.to_s.should eq "www.example.com"
    response.answers[0].data.to_s.should eq "1.2.3.4"
    server.close
  end
end
