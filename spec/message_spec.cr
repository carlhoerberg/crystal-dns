require "spec"
require "../src/dns"
include DNS

describe Message do
  it "serializes" do
    header = Header.new recursion_desired: true
    message = Message.new header: header, questions: [Question.new Name.new("example.com")]
    io = IO::Memory.new
    message.to_io io, IO::ByteFormat::BigEndian
    io.to_slice.hexstring.should eq "000001000001000000000000076578616d706c6503636f6d00001c0001"
    io.rewind
    message = Message.from_io io, IO::ByteFormat::BigEndian
    message.header.recursion_desired.should be_true
    message.questions.size.should eq 1
    message.questions[0].name.to_s.should eq "example.com"
  end
end
