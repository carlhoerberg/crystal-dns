class DNS::RR::WKS
  property address : IPv4Address
  property protocol : UInt8
  property bitmap : Bytes

  def initialize(@address = "",
                 @protocol = 0,
                 @bitmap = Bytes.new 0)
  end

  def to_io(io : IO, format : IO::ByteFormat)
    (@bitmap.size + 32 + 8).to_u16.to_io io, format
    @address.to_io(io, format, @address)
    @protocol.to_io(io, format)
    @bitmap.to_io(io, formats)
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    len = UInt16.from_io(io, format)
    self.new(
      address: IPv4Address.from_io(io, format),
      protocol: UInt8.from_io(io, format),
      bitmap: Util.bytes_from_io(io, format, len - 32 - 8),
    )
  end
end
