class DNS::RR::MX
  property preference : UInt16
  property exchange : Name

  def initialize(@preference = 0,
                 @exchange = Name.new)
  end

  def to_io(io : IO, format : IO::ByteFormat)
    (@exchange.bytesize + 2).to_u16.to_io io, format
    @preference.to_io(io, format)
    @exchange.to_io(io, format)
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    len = UInt16.from_io(io, format)
    self.new(
      preference: UInt16.from_io(io, format),
      exchange: Name.from_io(io, format),
    )
  end
end
