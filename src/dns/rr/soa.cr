class DNS::RR::SOA
  property mname : Name
  property rname : Name
  property serial : UInt32
  property refresh : UInt32
  property retry : UInt32
  property expire : UInt32
  property minimum : UInt32

  def initialize(@mname = Name.new,
                 @rname = Name.new,
                 @serial = 0_u32,
                 @refresh = 0_u32,
                 @retry = 0_u32,
                 @expire = 0_u32,
                 @minimum = 0_u32)
  end

  def to_io(io : IO, format : IO::ByteFormat)
    (@mname.bytesize + @rname.bytesize + (5*32)).to_u16.to_io(io, format)
    @mname.to_io(io, format)
    @rname.to_io(io, format)
    @serial.to_io(io, format)
    @refresh.to_io(io, format)
    @retry.to_io(io, format)
    @expire.to_io(io, format)
    @minimum.to_io(io, format)
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    len = UInt16.from_io(io, format)
    self.new(
      mname: Name.from_io(io, format),
      rname: Name.from_io(io, format),
      serial: UInt32.from_io(io, format),
      refresh: UInt32.from_io(io, format),
      retry: UInt32.from_io(io, format),
      expire: UInt32.from_io(io, format),
      minimum: UInt32.from_io(io, format),
    )
  end
end
