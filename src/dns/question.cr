class DNS::Question
  property name : Name
  property query_type : RecordType
  property query_class : RecordClass

  def initialize(@name = Name.new,
                 @query_type = RecordType::AAAA,
                 @query_class = RecordClass::Internet)
  end

  def initialize(name : String,
                 @query_type = RecordType::AAAA,
                 @query_class = RecordClass::Internet)
    @name = Name.new name
  end

  def to_io(io : IO, format : IO::ByteFormat)
    @name.to_io(io, format)
    @query_type.value.to_io(io, format)
    @query_class.value.to_io(io, format)
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    self.new(
      name: Name.from_io(io, format),
      query_type: RecordType.new(UInt16.from_io(io, format)),
      query_class: RecordClass.new(UInt16.from_io(io, format))
    )
  end
end
