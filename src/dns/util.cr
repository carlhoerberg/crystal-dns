module DNS
  module Util
    def self.string_from_io(io : IO, format : IO::ByteFormat)
      str = ""
      loop do
        codepoint = io.read_byte.not_nil!
        break if codepoint.zero?
        str += codepoint.chr
      end
      str
    end

    def self.string_to_io(io : IO, format : IO::ByteFormat, str : String)
      io.write(str.to_slice)
      0_u8.to_io(io, format)
    end
  end
end
