class DNS::Header
  property id : UInt16
  property message_type : MessageType
  property op_code : OpCode
  property authoritative : Bool
  property truncation : Bool
  property recursion_desired : Bool
  property recursion_available : Bool
  property response_code : ResponseCode
  property question_count : UInt16
  property answer_count : UInt16
  property name_server_count : UInt16
  property additional_count : UInt16

  def initialize(@message_type = MessageType::Query,
                 @op_code = OpCode::Query,
                 @id = 0_u16,
                 @authoritative = false,
                 @truncation = false,
                 @recursion_desired = false,
                 @recursion_available = false,
                 @response_code = ResponseCode::NoError,
                 @question_count = 0_u16,
                 @answer_count = 0_u16,
                 @name_server_count = 0_u16,
                 @additional_count = 0_u16)
  end

  # Returns `true` if the response code is not an error.
  def ok? : Bool
    @response_code == ResponseCode::NoError
  end

  def to_io(io : IO, format : IO::ByteFormat)
    @id.to_io io, format
    (@message_type.value << 15 |
      @op_code.value << 11 |
      (@authoritative ? 1 : 0) << 10 |
      (@truncation ? 1 : 0) << 9 |
      (@recursion_desired ? 1 : 0) << 8 |
      (@recursion_available ? 1 : 0) << 7 |
      @response_code.value).to_io io, format
    @question_count.to_io io, format
    @answer_count.to_io io, format
    @name_server_count.to_io io, format
    @additional_count.to_io io, format
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    id = UInt16.from_io io, format

    flags = format.decode UInt16, io
    message_type = MessageType.new flags >> 15
    op_code = OpCode.new (flags & 0x7800) >> 11
    authoritative = !((flags & 0x0400) >> 10).zero?
    truncation = !((flags & 0x0200) >> 9).zero?
    recursion_desired = !((flags & 0x0100) >> 8).zero?
    recursion_available = !((flags & 0x0080) >> 7).zero?
    response_code = ResponseCode.new (flags & 0x000f)

    question_count = UInt16.from_io(io, format)
    answer_count = UInt16.from_io(io, format)
    name_server_count = UInt16.from_io(io, format)
    additional_count = UInt16.from_io(io, format)

    self.new(
      id: id,
      message_type: message_type,
      op_code: op_code,
      authoritative: authoritative,
      truncation: truncation,
      recursion_desired: recursion_desired,
      recursion_available: recursion_available,
      response_code: response_code,
      question_count: question_count,
      answer_count: answer_count,
      name_server_count: name_server_count,
      additional_count: additional_count,
    )
  end
end
